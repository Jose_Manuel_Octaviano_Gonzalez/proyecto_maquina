#include "maquina.h"
#include <gtest/gtest.h>

TEST(SquareRootTest, PositiveNos){
    //ESTRADAS
    const char *entrada[] = {"NO_CARROS", "CARROS_ESTE", "CARROS_NORTE", "AMBOS_LADOS"};


    //ESTADOS
    const char *estado[] = {"goNorte", "waithNorte","goESTE","waithESTE"};
 

	const char *inputs[] = {"AMBOS_LADOS","NO_CARROS", "CARROS_ESTE", "CARROS_NORTE"};
    char estado_actual[20];

    //goNorte va bien solo el problema de NO_CARROS
    strcpy(estado_actual,"goNorte");
    ASSERT_STREQ("waithNorte", maquina(inputs, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"goNorte");
    ASSERT_STREQ("goNorte", maquina(inputs, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"goNorte");
    ASSERT_STREQ("waithNorte", maquina(inputs, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"goNorte");
    ASSERT_STREQ("goNorte", maquina(inputs, 3, estado_actual, estado, entrada));
    

    //waithNORTE
    strcpy(estado_actual,"waithNorte");
    ASSERT_STREQ("goESTE", maquina(inputs, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"waithNorte");
    ASSERT_STREQ("goESTE", maquina(inputs, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"waithNorte");
    ASSERT_STREQ("goESTE", maquina(inputs, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"waithNorte");
    ASSERT_STREQ("goESTE", maquina(inputs, 3, estado_actual, estado, entrada));

    //goESTE
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("waithESTE", maquina(inputs, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("goESTE", maquina(inputs, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("goESTE", maquina(inputs, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"goESTE");
    ASSERT_STREQ("waithESTE", maquina(inputs, 3, estado_actual, estado, entrada));

    //waithESTE
    strcpy(estado_actual,"waithESTE");
    ASSERT_STREQ("goNorte", maquina(inputs, 0, estado_actual, estado, entrada));
    strcpy(estado_actual,"waithESTE");
    ASSERT_STREQ("goNorte", maquina(inputs, 1, estado_actual, estado, entrada));
    strcpy(estado_actual,"waithESTE");
    ASSERT_STREQ("goNorte", maquina(inputs, 2, estado_actual, estado, entrada));
    strcpy(estado_actual,"waithESTE");
    ASSERT_STREQ("goNorte", maquina(inputs, 3, estado_actual, estado, entrada));

    
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}